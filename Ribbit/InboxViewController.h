//
//  InboxViewController.h
//  Ribbit
//
//  Created by Oscar Morrison on 22/09/13.
//  Copyright (c) 2013 ocodaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <MediaPlayer/MediaPlayer.h>

@interface InboxViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UILabel *userLabel;
@property(nonatomic,strong)NSArray *messages;
@property (strong, nonatomic) PFObject *selectedMessage;
@property(nonatomic,strong)MPMoviePlayerController *moviePlayer;

- (IBAction)logout:(id)sender;



@end

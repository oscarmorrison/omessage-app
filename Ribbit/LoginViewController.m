//
//  LoginViewController.m
//  Ribbit
//
//  Created by Oscar Morrison on 25/09/13.
//  Copyright (c) 2013 ocodaa. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>

@interface LoginViewController ()

@end

@implementation LoginViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    self.activity.hidden = TRUE;
}


- (IBAction)login:(id)sender {
    NSString *username = [self.usernameField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
     self.activity.hidden = FALSE;
    [self.activity startAnimating];
    
    if([username length] == 0 || [password length] == 0){
        UIAlertView *alertView = [[UIAlertView alloc]       initWithTitle:@"Oops"
                                                                  message:@"Make sure that you enter a username and password!"
                                                                 delegate:Nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
        [alertView show];
        self.activity.hidden = TRUE;
    }
    else{
        [PFUser logInWithUsernameInBackground:username password:password
                                        block:^(PFUser *user, NSError *error) {
                if (error) {
                    UIAlertView *alertView = [[UIAlertView alloc]       initWithTitle:@"Sorry"
                                                                              message: [error.userInfo objectForKey:@"error"]
                                                                             delegate:Nil
                                                                    cancelButtonTitle:@"OK"
                                                                    otherButtonTitles:nil];
                    [alertView show];
                    self.activity.hidden = TRUE;
                }
                else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
        
        // more sequential code that runs right away!
    }
    
}
@end

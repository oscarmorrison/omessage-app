//
//  FriendsViewController.h
//  Ribbit
//
//  Created by Oscar Morrison on 1/10/13.
//  Copyright (c) 2013 ocodaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface FriendsViewController : UITableViewController
@property(nonatomic,strong)PFRelation *friendsRelation;
@property(nonatomic,strong)NSArray *friends;
@end

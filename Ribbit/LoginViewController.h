//
//  LoginViewController.h
//  Ribbit
//
//  Created by Oscar Morrison on 25/09/13.
//  Copyright (c) 2013 ocodaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (IBAction)login:(id)sender;

@end

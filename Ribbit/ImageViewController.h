//
//  ImageViewController.h
//  Ribbit
//
//  Created by Oscar Morrison on 13/10/13.
//  Copyright (c) 2013 ocodaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ImageViewController : UIViewController
@property (nonatomic, strong) PFObject *message;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;


@end
